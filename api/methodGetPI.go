/*****************************************************************/
/* ***************************************************************/
/*                                                        ,,    **/
/* YMM'   `MM'                                          `7MM    **/
/*  VMA   ,V                                              MM    **/
/*   VMA ,V    ,6"Yb.  `7MMpdMAo.  ,pW"Wq.      ,p6"bo    MM    **/
/*    VMMP    8)   MM    MM   `Wb 6W'   `Wb    6M'  OO    MM    **/
/*     MM      ,pm9MM    MM    M8 8M     M8    8M         MM    **/
/*     MM     8M   MM    MM   ,AP YA.   ,A9 ,, YM.    ,   MM    **/
/*   .JMML.   `Moo9^Yo.  MMbmmd'   `Ybmd9'  db  YMbmd'  .JMML.  **/
/*                       MM                                     **/
/*                     .JMML.                                   **/
/* ***************************************************************/
/* ***************************************************************/
/*****************************************************************/
/*                         methodGetPI.go                        */
/*                                                               */
/* Descripcion: Contiene toda la logica del calculo y control de */
/*              la API.                                          */
/*                                                               */
/*  @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/*  @ Fecha  : 2021.05.29 13:46:00                               */
/*                                                               */
/* © 2021 - Yapo.cl - Desafio Tecnico - Technical Lead           */
/*****************************************************************/
package api

/**********************************/
/******** Import Packages *********/
/**********************************/
import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"

	"gitlab.com/rodrigoghm/ms-backend-api-get-pi/model"
	"gitlab.com/rodrigoghm/ms-backend-api-get-pi/public"

	"github.com/gorilla/mux"
)

/*
*
* @author   Rodrigo G. Higuera M. <rodrigoghm@gmail.com>
* @date     2021.05.29 00:34:06
* @version  20210529.0346.0001
* @Company  © Development 2021
*
* @func GetValuePI() : Funcion que contiene la logica del negocio para obtener los valores del numero PI
*
 */
func GetValuePI(w http.ResponseWriter, r *http.Request) {
	var R model.Response
	var E model.MsjError

	if os.Getenv("MODE_DEBUG") == "1" {
		v1 := fmt.Sprintf("function|%s", public.FuncInfo())
		public.WriteLog("|", "INFO", "::: Init Function :::", v1)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	params := mux.Vars(r)
	R.Vrandom = 0
	if vparam, err := strconv.Atoi(params["random"]); err == nil {
		R.Vparam = int64(vparam)
		v1 := fmt.Sprintf("valor|%d", R.Vparam)
		public.WriteLog("|", "INFO", "::: Init Function :::", v1)
		if R.Vparam == 0 {
			if os.Getenv("MODE_DEBUG") == "1" {
				v1 := fmt.Sprintf("function|%s", public.FuncInfo())
				public.WriteLog("|", "ERROR", "value zero not supported", v1)

				v2 := fmt.Sprintf("function|%s", public.FuncInfo())
				v3 := fmt.Sprintf("response_http|[409]")
				public.WriteLog("|", "ERROR", "::: End Function :::", v2, v3)
			}
			E = public.GetErrorHTTP409()
			w.WriteHeader(http.StatusConflict)
			json.NewEncoder(w).Encode(E)
			return
		}
		if R.Vparam > 1 {
			R.Vrandom = public.MakeRandom(1, R.Vparam)
			u, _ := strconv.ParseInt(os.Getenv("MAX_RANDOM_PRECISION"), 10, 64)
			if R.Vrandom > u {
				if os.Getenv("MODE_DEBUG") == "1" {
					v1 := fmt.Sprintf("function|%s", public.FuncInfo())
					public.WriteLog("|", "ERROR", "value random not supported", v1)

					v2 := fmt.Sprintf("function|%s", public.FuncInfo())
					v3 := fmt.Sprintf("response_http|[409]")
					public.WriteLog("|", "ERROR", "::: End Function :::", v2, v3)
				}
				E = public.GetErrorHTTP409()
				w.WriteHeader(http.StatusConflict)
				json.NewEncoder(w).Encode(E)
				return
			}
		} else {
			R.Vrandom = 1
		}
	} else {
		if os.Getenv("MODE_DEBUG") == "1" {
			v1 := fmt.Sprintf("function|%s", public.FuncInfo())
			public.WriteLog("|", "ERROR", "Value no numeric", v1)

			v2 := fmt.Sprintf("function|%s", public.FuncInfo())
			v3 := fmt.Sprintf("response_http|[400]")
			public.WriteLog("|", "ERROR", "::: End Function :::", v2, v3)
		}
		R.Vparam = 0
		E = public.GetErrorHTTP400()
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(E)
		return
	}

	R.VpiCalc = public.GetPIWithMachinForm(R.Vrandom)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(R)
	if os.Getenv("MODE_DEBUG") == "1" {
		v2 := fmt.Sprintf("function|%s", public.FuncInfo())
		v3 := fmt.Sprintf("var|['%s']---[%d]", "param", R.Vparam)
		public.WriteLog("|", "INFO", "", v2, v3)
		v3 = fmt.Sprintf("var|['%s']---[%d]", "random", R.Vrandom)
		public.WriteLog("|", "INFO", "", v2, v3)
		v3 = fmt.Sprintf("var|['%s']---[%s]", "PiCalc", R.VpiCalc)
		public.WriteLog("|", "INFO", "", v2, v3)
		v1 := fmt.Sprintf("function|%s", public.FuncInfo())
		public.WriteLog("|", "INFO", "::: End Function :::", v1)
	}
	return
}
