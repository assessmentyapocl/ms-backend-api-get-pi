/*****************************************************************/
/* ***************************************************************/
/*                                                        ,,    **/
/* YMM'   `MM'                                          `7MM    **/
/*  VMA   ,V                                              MM    **/
/*   VMA ,V    ,6"Yb.  `7MMpdMAo.  ,pW"Wq.      ,p6"bo    MM    **/
/*    VMMP    8)   MM    MM   `Wb 6W'   `Wb    6M'  OO    MM    **/
/*     MM      ,pm9MM    MM    M8 8M     M8    8M         MM    **/
/*     MM     8M   MM    MM   ,AP YA.   ,A9 ,, YM.    ,   MM    **/
/*   .JMML.   `Moo9^Yo.  MMbmmd'   `Ybmd9'  db  YMbmd'  .JMML.  **/
/*                       MM                                     **/
/*                     .JMML.                                   **/
/* ***************************************************************/
/* ***************************************************************/
/*****************************************************************/
/*                         goFile.go                             */
/*                                                               */
/* Descripcion: Template de creación de ficheros go nuevos para  */
/*              desarrollar.                                     */
/*                                                               */
/*  @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/*  @ Fecha  : 2021.05.29 13:46:00                               */
/*                                                               */
/* © 2021 - Yapo.cl - Desafio Tecnico - Technical Lead           */
/*****************************************************************/
package main

/**********************************/
/******** Import Packages *********/
/**********************************/
import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"time"
)

const (
	layoutISO = "2006-01-02 13:21:00"
)

/*****************************************************/
/****** Definicion de Constantes Globales - Fin ******/
/*****************************************************/

/*****************************************************/
/****** Definicion de Variables Globales - Inicio ****/
/*****************************************************/
var author string = "Rodrigo G. Higuera M. <rodrigoghm@gmail.com>"
var fechaSystem, Y string
var versionFile string
var PathFile string = "/home/higuerar/go/src/yapo-test/ms-backend-api-get-pi/"
var nameFile string
var cl int = 0

/*****************************************************/
/******* Definicion de Variables Globales - Fin ******/
/*****************************************************/

func writeFile(line string) {
	if _, err := os.Stat(PathFile); os.IsNotExist(err) {
		fmt.Println("def")
		f, err := os.Create(PathFile)
		if err != nil {
			fmt.Println("Error ", err)
			f.Close()
			return
		}

		fmt.Fprintln(f, line)
	} else {
		// -- Si el fichero ya existe.
		x := fmt.Sprintf("cat %s | wc -l", PathFile)
		out, err := exec.Command("bash", "-c", x).Output()
		if err != nil {
			fmt.Printf("Error ==> [%s]", err)
		}
		fmt.Println("OUT===>" + string(out))

		line := line + " " + string(out)
		// -- open input file
		f, err := os.OpenFile(PathFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Println(err)
		}

		// -- close fi on exit and check for its returned error
		defer func() {
			if err := f.Close(); err != nil {
				panic(err)
			}
		}()

		if _, err := f.WriteString(line); err != nil {
			log.Println(err)
		}
	}
}

func completaEspaciosString(line string, car byte) string {
	maxLine := 78

	for i := len(line); i < maxLine; i++ {
		line = line + string(car)
	}

	return line
}

func PrintLineFichero(line, fichero string) {
	var c string
	if cl == 0 {
		c = fmt.Sprintf("echo %s > %s", line, fichero)
	} else {
		c = fmt.Sprintf("echo %s >> %s", line, fichero)
	}

	_, err := exec.Command("bash", "-c", c).Output()
	if err != nil {
		fmt.Printf("Error ==> [%s]", err)
	} else {
		cl++
	}
}

func printHeaderFileGo(fichero string) {
	lineAuthor := completaEspaciosString("  @ Autor  : "+author, ' ')
	lineFecha := completaEspaciosString("  @ Fecha  : "+fechaSystem, ' ')
	lineTitle := completaEspaciosString("                              "+nameFile, ' ')
	PrintLineFichero("\"/********************************************************************************/\"", fichero)
	PrintLineFichero("\"/*                                                                              */\"", fichero)
	PrintLineFichero("\"/*"+lineTitle+"*/\"", fichero)
	PrintLineFichero("\"/*                                                                              */\"", fichero)
	PrintLineFichero("\"/* +++ Descripcion:                                                             */\"", fichero)
	PrintLineFichero("\"/*                                                                              */\"", fichero)
	PrintLineFichero("\"/*                                                                              */\"", fichero)
	PrintLineFichero("\"/*"+lineAuthor+"*/\"", fichero)
	PrintLineFichero("\"/*"+lineFecha+"*/\"", fichero)
	PrintLineFichero("\"/*                                                                              */\"", fichero)
	PrintLineFichero("\"/* © 2021 - Yapo.cl - Desafio Tecnico                                           */\"", fichero)
	PrintLineFichero("\"/********************************************************************************/\"", fichero)
	PrintLineFichero("\"/********************************************************************************/\"", fichero)
	PrintLineFichero("\"/* Ejecucion :                                                                  */\"", fichero)
	PrintLineFichero("\"/*                                                                              */\"", fichero)
	PrintLineFichero("\"/*                                                                              */\"", fichero)
	PrintLineFichero("\"/********************************************************************************/\"", fichero)
	PrintLineFichero("\"\"", fichero)

}

func printBodyFileGo(fichero string) {
	strVersion := fmt.Sprintf("%s", versionFile)
	packImport := fmt.Sprintf("'\t''\"%s\"'", "fmt")
	PrintLineFichero("\"package main\"", fichero)
	PrintLineFichero("\"\"", fichero)
	PrintLineFichero("\"/**********************************/\"", fichero)
	PrintLineFichero("\"/******** Import Packages *********/\"", fichero)
	PrintLineFichero("\"/**********************************/\"", fichero)
	PrintLineFichero("\"import (\"", fichero)
	PrintLineFichero(packImport, fichero)
	PrintLineFichero("\")\"", fichero)
	PrintLineFichero("\"\"", fichero)
	PrintLineFichero("\"\"", fichero)
	PrintLineFichero("\"/*****************************************************/\"", fichero)
	PrintLineFichero("\"/******** Definicion de Estructuras - Inicio *********/\"", fichero)
	PrintLineFichero("\"/*****************************************************/\"", fichero)
	PrintLineFichero("\"\"", fichero)
	PrintLineFichero("\"/*****************************************************/\"", fichero)
	PrintLineFichero("\"/******** Definicion de Estructuras - Fin ************/\"", fichero)
	PrintLineFichero("\"/*****************************************************/\"", fichero)
	PrintLineFichero("\"\"", fichero)
	PrintLineFichero("\"\"", fichero)
	PrintLineFichero("\"/*****************************************************/\"", fichero)
	PrintLineFichero("\"/***** Definicion de Constantes Globales - Inicio ****/\"", fichero)
	PrintLineFichero("\"/*****************************************************/\"", fichero)
	PrintLineFichero("   const VERSION = '\""+strVersion+"\"'", fichero)
	PrintLineFichero("\"/*****************************************************/\"", fichero)
	PrintLineFichero("\"/***** Definicion de Constantes Globales - Fin *******/\"", fichero)
	PrintLineFichero("\"/*****************************************************/\"", fichero)
	PrintLineFichero("\"\"", fichero)
	PrintLineFichero("\"\"", fichero)
	PrintLineFichero("\"/*****************************************************/\"", fichero)
	PrintLineFichero("\"/****** Definicion de Variables Globales - Inicio ****/\"", fichero)
	PrintLineFichero("\"/*****************************************************/\"", fichero)
	PrintLineFichero("\"\"", fichero)
	PrintLineFichero("\"/*****************************************************/\"", fichero)
	PrintLineFichero("\"/****** Definicion de Variables Globales - Fin ******/\"", fichero)
	PrintLineFichero("\"/*****************************************************/\"", fichero)
	PrintLineFichero("\"\"", fichero)
	PrintLineFichero("\"\"", fichero)
	PrintLineFichero("\"/*\"", fichero)
	PrintLineFichero("\"*\"", fichero)
	PrintLineFichero("\"* @author   "+author+"\"", fichero)
	PrintLineFichero("\"* @date     "+fechaSystem+"\"", fichero)
	PrintLineFichero("\"* @version  "+versionFile+"\"", fichero)
	PrintLineFichero("\"* @Company  © Development "+Y+"\"", fichero)
	PrintLineFichero("\"*\"", fichero)
	PrintLineFichero("\"*/\"", fichero)
	PrintLineFichero("\"func main() {\"", fichero)
	PrintLineFichero("\"\"", fichero)
	PrintLineFichero("\"}\"", fichero)
	PrintLineFichero("\"\"", fichero)
}

/*
*
* @author   Rodrigo G. Higuera M.(RGHM) <rodrigoghm@gmail.com>
* @date     2021.05.28 17:19
* @version  20210528.171900.0001
* @Company  © Development 2021
*
 */
func main() {
	fmt.Println("::: Inicio :::")

	dt := time.Now()
	fechaSystem = dt.Format("2006.01.02 15:04:05")
	v1 := string(dt.Format("20060102"))
	v2 := strconv.Itoa(dt.Hour()) + strconv.Itoa(dt.Minute()) + strconv.Itoa(dt.Second())
	Y = strconv.Itoa(dt.Year())
	versionFile = v1 + "." + v2 + "." + "0001"

	// -- se recibe nombre por parametro de nombre de fichero
	argCount := len(os.Args[1:])
	if argCount >= 1 {
		fn := os.Args[1]
		// -- fmt.Println(fn)
		nameFile = fn
	} else {
		nameFile = "nuevo_" + string(dt.Format("20060102")) + ".go"
	}

	printHeaderFileGo(PathFile + nameFile)
	printBodyFileGo(PathFile + nameFile)
	fmt.Println("::: Fin :::")
}
