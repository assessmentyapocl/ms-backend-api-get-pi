# -- ***************************************************************
# -- ***************************************************************
# --                                                        ,,    **
# -- YMM'   `MM'                                          `7MM    **
# --  VMA   ,V                                              MM    **
# --   VMA ,V    ,6"Yb.  `7MMpdMAo.  ,pW"Wq.      ,p6"bo    MM    **
# --    VMMP    8)   MM    MM   `Wb 6W'   `Wb    6M'  OO    MM    **
# --     MM      ,pm9MM    MM    M8 8M     M8    8M         MM    **
# --     MM     8M   MM    MM   ,AP YA.   ,A9 ,, YM.    ,   MM    **
# --   .JMML.   `Moo9^Yo.  MMbmmd'   `Ybmd9'  db  YMbmd'  .JMML.  **
# --                       MM                                     **
# --                     .JMML.                                   **
# -- ***************************************************************
# -- ***************************************************************
# /*****************************************************************/
# /*                         Makefile                              */
# /*                                                               */
# /* Descripcion: Archivo que permitira excluir archivos a         */
# /*              subir al repositorio remoto.                     */
# /*                                                               */
# /*  @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
# /*  @ Fecha  : 2021.05.29 13:50:00                               */
# /*                                                               */
# /* © Yapo.cl 2021                                                */
# /*****************************************************************/
FROM rodrigoghm/golang:1.16.4
RUN mkdir -p /go/src//ms-backend-api-get-pi
WORKDIR /go/src/ms-backend-api-get-pi
COPY . /go/src/ms-backend-api-get-pi
# Configure Go
ENV GOROOT /usr/local/go/
ENV GOPATH /go/src
ENV PATH /go/bin:$PATH
RUN make install
EXPOSE 3001
# Run the binary program produced by 'make install'
CMD ["make", "up"]
