/*****************************************************************/
/* ***************************************************************/
/*                                                        ,,    **/
/* YMM'   `MM'                                          `7MM    **/
/*  VMA   ,V                                              MM    **/
/*   VMA ,V    ,6"Yb.  `7MMpdMAo.  ,pW"Wq.      ,p6"bo    MM    **/
/*    VMMP    8)   MM    MM   `Wb 6W'   `Wb    6M'  OO    MM    **/
/*     MM      ,pm9MM    MM    M8 8M     M8    8M         MM    **/
/*     MM     8M   MM    MM   ,AP YA.   ,A9 ,, YM.    ,   MM    **/
/*   .JMML.   `Moo9^Yo.  MMbmmd'   `Ybmd9'  db  YMbmd'  .JMML.  **/
/*                       MM                                     **/
/*                     .JMML.                                   **/
/* ***************************************************************/
/* ***************************************************************/
/*****************************************************************/
/*                         methodGetPI.go                        */
/*                                                               */
/* Descripcion: Modelo/Estructuras a implementar en la API.      */
/*                                                               */
/*  @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/*  @ Fecha  : 2021.05.28 23:46:00                               */
/*                                                               */
/* © 2021 - Yapo.cl - Desafio Tecnico - Technical Lead           */
/*****************************************************************/
package model

/*****************************************************/
/******** Definicion de Estructuras - Inicio *********/
/*****************************************************/
type Response struct {
	Vparam  int64  `json:"param"`
	Vrandom int64  `json:"random"`
	VpiCalc string `json:"PiCalc"`
}

type MsjError struct {
	Description     string `json:"description"`
	UserMessage     string `json:"userMessage"`
	InternalMessage string `json:"internalMessage"`
	MoreInfo        string `json:"moreInfo"`
}

/*****************************************************/
/******** Definicion de Estructuras - Fin ************/
/*****************************************************/
