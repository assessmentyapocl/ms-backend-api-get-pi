/********************************************************************************/
/*                                                                              */
/*                                  utils.go                                    */
/*                                                                              */
/* +++ Descripcion: Libreria personal de funciones necesarias para el correcto */
/*     funcionamiento de la API.                                                */
/*                                                                              */
/*                                                                              */
/*  @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>                     */
/*  @ Fecha  : 2021.05.29 01:44:37                                              */
/*                                                                              */
/* © 2021 - Yapo - Desafio tecnico.                                             */
/********************************************************************************/
/********************************************************************************/
/* Ejecucion :                                                                  */
/*                                                                              */
/*                                                                              */
/********************************************************************************/
package public

/**********************************/
/******** Import Packages *********/
/**********************************/

import (
	"crypto/rand"
	"fmt"
	"math"
	"math/big"
	"runtime"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/rodrigoghm/ms-backend-api-get-pi/model"
)

/*
*
* @author   Rodrigo G. Higuera M. <rodrigoghm@gmail.com>
* @date     2021.05.29 01:44:37
* @version  20210529.14437.0001
* @Company  © Development 2021
*
* @func MakeRandom() : Funcion que permite realizar el calculo de numeros aleatorios en Golang.
*			           * min  <int64> : Valor inicial del valor de rango a obtener un numero aleatorio.
*			           * max  <int64> : Valor final del valor de rango a obtener un numero aleatorio.
 */
func MakeRandom(min, max int64) int64 {
	n, err := rand.Int(rand.Reader, big.NewInt(max-min))
	if err != nil {
		panic(err)
	}

	return n.Int64() + min
}

/*
*
* @author   Rodrigo G. Higuera M. <rodrigoghm@gmail.com>
* @date     2021.05.29 01:44:37
* @version  20210529.14437.0001
* @Company  © Development 2021
*
* @func arctan() :
*
*            Funcion que permite realizar el calculo de decimales de PI basado en la maquina de Machin
*            Es uno de los algoritmos mas utilizados para calcular con eficiencia la precisión de los valores
*            decimales de PI. Para el caso de Go se emplea el tipo de dato Big para evitar desbordamientos.
*
*           Comenzamos definiendo un arco cotangente de alta precisión. función. Este devuelve la respuesta como un
*           número entero normalmente sería un número de coma flotante. Aquí, el número entero se multiplica por la
*           "unidad" que pasamos.Si la unidad es 10, por ejemplo, y la respuesta debería ser "0.5", entonces la
*           respuesta saldrá como 5.
 */
func arctan(x int64, unity *big.Int) *big.Int {
	bigx := big.NewInt(x)
	xsquared := big.NewInt(x * x)
	sum := big.NewInt(0)
	sum.Div(unity, bigx)
	xpower := big.NewInt(0)
	xpower.Set(sum)
	n := int64(3)
	zero := big.NewInt(0)
	sign := false

	term := big.NewInt(0)
	for {
		xpower.Div(xpower, xsquared)
		term.Div(xpower, big.NewInt(n))
		if term.Cmp(zero) == 0 {
			break
		}
		if sign {
			sum.Add(sum, term)
		} else {
			sum.Sub(sum, term)
		}
		sign = !sign
		n += 2
	}
	return sum
}

/*
*
* @author   Rodrigo G. Higuera M. <rodrigoghm@gmail.com>
* @date     2021.05.29 01:44:37
* @version  20210529.14437.0001
* @Company  © Development 2021
*
* @func GetPIWithMachinForm() : Funcion que implementa la abstracción del calculo de PI.
*			                   * p  <int64> : Valor de precision de decimales que se desea obtener.
 */
func GetPIWithMachinForm(p int64) string {
	digits := big.NewInt(p + 10)
	unity := big.NewInt(0)
	unity.Exp(big.NewInt(10), digits, nil)
	pi := big.NewInt(0)
	four := big.NewInt(4)
	pi.Mul(four, pi.Sub(pi.Mul(four, arctan(5, unity)), arctan(239, unity)))
	if p < 15 {
		parcial := pi.String()[0:1] + "." + pi.String()[1:p+2]
		return getRedondeaUltimoDigito(parcial)
	}
	return pi.String()[0:1] + "." + pi.String()[1:1+p]
}

/*
*
* @author   Rodrigo G. Higuera M. <rodrigoghm@gmail.com>
* @date     2021.06.01 01:44:37
* @version  20210601.014437.0001
* @Company  © Development 2021
*
* @func getRedondeaUltimoDigito() : Redondea el ultimo digito de los digitos de precision.
 */
func getRedondeaUltimoDigito(s string) string {
	x := int64(len(s))
	ult, _ := strconv.Atoi(string(s[x-2]))
	sig, _ := strconv.Atoi(string(s[x-1]))
	if sig >= 5 {
		if ult = ult + 1; ult == 10 {
			ult = 0
		}
	}
	return s[0:x-2] + strconv.Itoa(int(ult))
}

/*
*
* @author   Rodrigo G. Higuera M. <rodrigoghm@gmail.com>
* @date     2021.05.29 01:44:37
* @version  20210529.14437.0001
* @Company  © Development 2021
*
* @func GetMathPi() : Funcion que implementa el calculo de PI.
 */
func GetMathPi(p int, strPI string) string {
	s := strconv.FormatFloat(math.Pi, 'f', -15, 64)
	//fmt.Println("Recibido <p> => [" + strconv.Itoa(p) + "> | <strPI> => [" + strPI + "]")
	if p < 15 {
		parcial := s[0:1] + s[1:p+1]
		//fmt.Println(parcial)
		ult, _ := strconv.Atoi(string(s[p+1]))
		//fmt.Println(ult)
		sig, _ := strconv.Atoi(string(s[p+2]))
		//fmt.Println(sig)
		if sig >= 5 {
			ult = ult + 1
		}
		parcial = parcial + strconv.Itoa(ult)
		return parcial
	}
	return strPI[0:2] + strPI[2:2+p]
}

/*
*
* @author   Rodrigo G. Higuera M.(RGHM) <rodrigoghm@gmail.com>
* @date     2020.10.12 16:52
* @version  20201012.165232.0001
* @Company  © Development 2020
*
* @func WriteLog : Funcion variadica que permite el registro de logs.
*			* car  <string> : Caracter por el cual se separara los valores {campo-valor}.
*			* tlog <string> : Tipo de log a registrar. Solo validos {ERROR, INFO, WARNING}
*			* msg  <string> : Mensaje del log.
*			* vals <...string> : Valores variadicos con la relacion {campo-valor}.
 */
func WriteLog(car, tlog, msg string, vals ...string) {
	log.SetFormatter(&log.JSONFormatter{})

	cl := log.WithFields(
		log.Fields{})
	if tlog == "INFO" {

		for _, val := range vals {
			arr := strings.Split(val, car)
			cl = cl.WithFields(
				log.Fields{
					arr[0]: arr[1],
				})
		}
		cl.Info(msg)
	} else if tlog == "ERROR" {
		for _, val := range vals {
			arr := strings.Split(val, car)
			cl = cl.WithFields(
				log.Fields{
					arr[0]: arr[1],
				})
		}
		cl.Error(msg)
	} else {
		for _, val := range vals {
			arr := strings.Split(val, car)
			cl = cl.WithFields(
				log.Fields{
					arr[0]: arr[1],
				})
		}
		cl.Warn(msg)
	}

}

/*
*
* @author   Rodrigo G. Higuera M.(RGHM) <rodrigoghm@gmail.com>
* @date     2020.10.12 16:52
* @version  20201012.165232.0001
* @Company  © Development 2020
*
* @func FileInfo : Funcion que devuelve información del fichero que se esta ejecutando
 */
func FileInfo(skip int) string {
	_, file, line, ok := runtime.Caller(skip)
	if !ok {
		file = "<???>"
		line = 1
	} else {
		slash := strings.LastIndex(file, "/")
		if slash >= 0 {
			file = file[slash+1:]
		}
	}

	return fmt.Sprintf("%s:%d", file, line)
}

/*
*
* @author   Rodrigo G. Higuera M.(RGHM) <rodrigoghm@gmail.com>
* @date     2020.10.12 16:52
* @version  20201012.165232.0001
* @Company  © Development 2020
*
* @func FuncInfo : Funcion que devuelve el nombre de la funcion que se esta ejecutando.
 */
func FuncInfo() string {
	pc, _, _, _ := runtime.Caller(1)
	funcname := runtime.FuncForPC(pc).Name()
	fn := funcname[strings.LastIndex(funcname, "/")+1:]
	return fmt.Sprintf("%s()", fn)
}

/*
*
* @author   Rodrigo G. Higuera M.(RGHM) <rodrigoghm@gmail.com>
* @date     2020.10.12 16:52
* @version  20210530.165232.0001
* @Company  © Development 2020
*
* @func GetErrorHTTP400() : Funcion que devuelve la estructura de error cargada segun el error
*                           HTTP Response 400.
 */
func GetErrorHTTP400() model.MsjError {
	var ERR model.MsjError

	ERR.Description = "The request could not be understood by the server due to malformed syntax. The client SHOULD NOT repeat the request without modifications."
	ERR.InternalMessage = "Bad Request"
	ERR.MoreInfo = "https://httpstatuses.com/400"
	ERR.UserMessage = "Request Not Valid"

	return ERR
}

/*
*
* @author   Rodrigo G. Higuera M.(RGHM) <rodrigoghm@gmail.com>
* @date     2020.10.12 16:52
* @version  20210530.165232.0001
* @Company  © Development 2020
*
* @func GetErrorHTTP409() : Funcion que devuelve la estructura de error cargada segun el error
*                           HTTP Response 409.
 */
func GetErrorHTTP409() model.MsjError {
	var ERR model.MsjError

	ERR.Description = "The request could not be completed due to a conflict with the current state of the resource. This code is only allowed in situations where it is expected that the user might be able to resolve the conflict and resubmit the request. The response body SHOULD include enough <br /> information for the user to recognize the source of the conflict. Ideally, the response entity would include enough information for the user or user agent to fix the problem; however, that might not be possible and is not required. <br />Conflicts are most likely to occur in response to a PUT request. For example, if versioning were being used and the entity being PUT included changes to a resource which conflict with those made by an earlier (third-party) request, the server might use the 409 response to indicate that it can't complete the request. In this case, the response entity would likely contain a list of the differences between the two versions in a format defined by the response Content-Type"
	ERR.InternalMessage = "Conflict"
	ERR.MoreInfo = "https://httpstatuses.com/409"
	ERR.UserMessage = "Random parameter would cause overflow"

	return ERR
}
