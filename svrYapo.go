/* ***************************************************************/
/* ***************************************************************/
/*                                                        ,,    **/
/* YMM'   `MM'                                          `7MM    **/
/*  VMA   ,V                                              MM    **/
/*   VMA ,V    ,6"Yb.  `7MMpdMAo.  ,pW"Wq.      ,p6"bo    MM    **/
/*    VMMP    8)   MM    MM   `Wb 6W'   `Wb    6M'  OO    MM    **/
/*     MM      ,pm9MM    MM    M8 8M     M8    8M         MM    **/
/*     MM     8M   MM    MM   ,AP YA.   ,A9 ,, YM.    ,   MM    **/
/*   .JMML.   `Moo9^Yo.  MMbmmd'   `Ybmd9'  db  YMbmd'  .JMML.  **/
/*                       MM                                     **/
/*                     .JMML.                                   **/
/* ***************************************************************/
/* ***************************************************************/
/*****************************************************************/
/*                         Makefile                              */
/*                                                               */
/* Descripcion: Servidor del aplicativo.                         */
/*                                                               */
/*  @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/*  @ Fecha  : 2021.05.29 13:46:00                               */
/*                                                               */
/* © Yapo.cl 2021                                                */
/*****************************************************************/
package main

/**********************************/
/******** Import Packages *********/
/**********************************/
import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gitlab.com/rodrigoghm/ms-backend-api-get-pi/api"
)

/*
*
* @author   Rodrigo G. Higuera M. <rodrigoghm@gmail.com>
* @date     2021.05.28 18:40:52
* @version  20210528.184052.0001
* @Company  © Development 2021
*
* printHeader() : Funcion que muestra el texto de Yapo.cl Solo cuando inicia el servidor.
*
 */
func printHeader() {
	fmt.Println("")
	fmt.Println("")
	fmt.Println("")
	fmt.Println("YYYYYY       YYYYYYY                                                                                   lllllll  ")
	fmt.Println("Y:::::Y       Y:::::Y                                                                                   l:::::l ")
	fmt.Println("Y:::::Y       Y:::::Y                                                                                   l:::::l ")
	fmt.Println("Y::::::Y     Y::::::Y                                                                                   l:::::l ")
	fmt.Println("YYY:::::Y   Y:::::YYY  aaaaaaaaaaaaa   ppppp   ppppppppp      ooooooooooo               cccccccccccccccc l::::l ")
	fmt.Println("   Y:::::Y Y:::::Y     a::::::::::::a  p::::ppp:::::::::p   oo:::::::::::oo           cc:::::::::::::::c l::::l ")
	fmt.Println("    Y:::::Y:::::Y      aaaaaaaaa:::::a p:::::::::::::::::p o:::::::::::::::o         c:::::::::::::::::c l::::l ")
	fmt.Println("     Y:::::::::Y                a::::a pp::::::ppppp::::::po:::::ooooo:::::o        c:::::::cccccc:::::c l::::l ")
	fmt.Println("      Y:::::::Y          aaaaaaa:::::a  p:::::p     p:::::po::::o     o::::o        c::::::c     ccccccc l::::l ")
	fmt.Println("       Y:::::Y         aa::::::::::::a  p:::::p     p:::::po::::o     o::::o        c:::::c              l::::l ")
	fmt.Println("       Y:::::Y        a::::aaaa::::::a  p:::::p     p:::::po::::o     o::::o        c:::::c              l::::l ")
	fmt.Println("       Y:::::Y       a::::a    a:::::a  p:::::p    p::::::po::::o     o::::o        c::::::c     ccccccc l::::l ")
	fmt.Println("       Y:::::Y       a::::a    a:::::a  p:::::ppppp:::::::po:::::ooooo:::::o        c:::::::cccccc:::::cl::::::l")
	fmt.Println("    YYYY:::::YYYY    a:::::aaaa::::::a  p::::::::::::::::p o:::::::::::::::o ......  c:::::::::::::::::cl::::::l")
	fmt.Println("    Y:::::::::::Y     a::::::::::aa:::a p::::::::::::::pp   oo:::::::::::oo  .::::.   cc:::::::::::::::cl::::::l")
	fmt.Println("    YYYYYYYYYYYYY      aaaaaaaaaa  aaaa p::::::pppppppp       ooooooooooo    ......     ccccccccccccccccllllllll")
	fmt.Println("                                        p:::::p                                                                 ")
	fmt.Println("                                        p:::::p                                                                 ")
	fmt.Println("                                       p:::::::p                                                                ")
	fmt.Println("                                       p:::::::p                                                                ")
	fmt.Println("                                       p:::::::p                                                                ")
	fmt.Println("                                       ppppppppp                                                                ")
	fmt.Println("")
	fmt.Println("")
	fmt.Println("")
}

/*
*
* @author   Rodrigo G. Higuera M. <rodrigoghm@gmail.com>
* @date     2021.05.28 18:40:52
* @version  20210528.184052.0001
* @Company  © Development 2021
*
* init() : Caller previo al inicio del codigo analiza las variables de entorno.
 */
func init() {

	_ = godotenv.Load(".env")

	if os.Getenv("MODE_DEBUG") == "" {
		errX := os.Setenv("MODE_DEBUG", "1")
		if errX != nil {
			log.Fatal(errX)
		}
	}

	if os.Getenv("PORT_SERVER") == "" {
		errX := os.Setenv("PORT_SERVER", "3002")
		if errX != nil {
			log.Fatal(errX)
		}
	}

	if os.Getenv("LANGUAGE") == "" {
		errX := os.Setenv("LANGUAGE", "Go")
		if errX != nil {
			log.Fatal(errX)
		}
	}

	if os.Getenv("MAX_RANDOM_PRECISION") == "" {
		errX := os.Setenv("MAX_RANDOM_PRECISION", "350000")
		if errX != nil {
			log.Fatal(errX)
		}
	}
}

/*
*
* @author   Rodrigo G. Higuera M. <rodrigoghm@gmail.com>
* @date     2021.05.28 21:00:11
* @version  20210528.21011.0001
* @Company  © Development 2021
*
 */
func main() {
	// -- call printHeader
	printHeader()

	// -- get variables
	fmt.Println("****************************************************************************")
	fmt.Println("****************************************************************************")
	fmt.Printf("PORT_SERVER ==> [%v]\n", os.Getenv("PORT_SERVER"))
	fmt.Printf("LANGUAGE    ==> [%v]\n", os.Getenv("LANGUAGE"))
	fmt.Printf("MODE_DEBUG  ==> [%v]\n", os.Getenv("MODE_DEBUG"))
	fmt.Println("****************************************************************************")
	fmt.Println("****************************************************************************")
	fmt.Println("Server start: 🛸 ... π")

	// -- Init Router
	router := mux.NewRouter()

	// -- Endpoints
	router.HandleFunc("/api/v1/getPI/{random}", api.GetValuePI).Methods("GET")

	// -- Open Port - Server up
	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT_SERVER"), router))
}
