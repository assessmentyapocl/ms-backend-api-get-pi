# -- ***************************************************************
# -- ***************************************************************
# --                                                        ,,    **
# -- YMM'   `MM'                                          `7MM    **
# --  VMA   ,V                                              MM    **
# --   VMA ,V    ,6"Yb.  `7MMpdMAo.  ,pW"Wq.      ,p6"bo    MM    **
# --    VMMP    8)   MM    MM   `Wb 6W'   `Wb    6M'  OO    MM    **
# --     MM      ,pm9MM    MM    M8 8M     M8    8M         MM    **
# --     MM     8M   MM    MM   ,AP YA.   ,A9 ,, YM.    ,   MM    **
# --   .JMML.   `Moo9^Yo.  MMbmmd'   `Ybmd9'  db  YMbmd'  .JMML.  **
# --                       MM                                     **
# --                     .JMML.                                   **
# -- ***************************************************************
# -- ***************************************************************
# /*****************************************************************/
# /*                         Makefile                              */
# /*                                                               */
# /* Descripcion: Archivo que permitira excluir archivos a         */
# /*              subir al repositorio remoto.                     */
# /*                                                               */
# /*  @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
# /*  @ Fecha  : 2021.05.29 13:46:00                               */
# /*                                                               */
# /* © Yapo.cl 2021                                                */
# /*****************************************************************/

# Comandos Go
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test ./...
GOGET=$(GOCMD) get -u -v

PATHABSBIN=/go/src/ms-backend-api-get-pi/bin/
PATHBIN=bin/
PROJECTNAME=svrYapo
LOGPROJECT=/tmp/$(PROJECTNAME).log
BINARY=$(PATHBIN)$(PROJECTNAME)
EXEBINARY=$(PATHABSBIN)$(PROJECTNAME)
MAIN=svrYapo.go

# PID file will keep the process id of the server
PID=/tmp/.$(PROJECTNAME).pid

# -- Dependencias del Aplicativo
go-get:
	$(GOGET) github.com/gorilla/mux
	$(GOGET) github.com/sirupsen/logrus
	$(GOGET) github.com/joho/godotenv

# -- Creacion de directorio bin
go-dir: 
	mkdir -p $(PATHBIN)

# -- Compilacion del proyecto
build: go-dir
	go build -o $(BINARY) $(MAIN)
	
# -- Compilacion Cross-Platform
compile: go-dir
	# -- 32 Bit System
	# Linux
	#GOOS=linux GOARCH=386 go build -o $(BINARY)-linux-386 $(MAIN)
	# Windows
	#GOOS=windows GOARCH=386 go build -o $(BINARY)-windows-386 $(MAIN)
	# -- 64 Bit System
	# Linux
	GOOS=linux GOARCH=amd64 go build -o $(BINARY)-linux-amd64 $(MAIN)
	# Windows
	GOOS=windows GOARCH=amd64 go build -o $(BINARY)-windows-amd64 $(MAIN)

# -- Ejecucion del proyecto
run:
	go run $(MAIN)
	
# -- Instalacion del Proyecto
install: go-get build

# -- Inicia servidor 
start-server: stop-server
	@-pwd
	@-$(BINARY) 2>&1 & echo $$! > $(PID)
	@cat $(PID) | sed "/^/s/^/  \>  PID: /"

# -- Detiene Servidor
stop-server:
	@-touch $(PID)
	@-kill `cat $(PID)` 2>/dev/null || true
	@-rm -f $(PID)
	
# -- Reiniciar servidor
restart-server: stop-server start-server

# -- Comando personalizado - Local
up-local: start-server

# -- Comando personalizado - Docker
up: 
	$(BINARY)
	
# -- Desde compilacion del proyecto hasta subida del servicio
all: clean install up

# -- Limpieza del Proyecto
clean:
	$(GOCLEAN)
	@rm -rf $(PATHBIN)*
