module gitlab.com/rodrigoghm/ms-backend-api-get-pi

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/sys v0.0.0-20210525143221-35b2ab0089ea // indirect
	golang.org/x/tools v0.1.2 // indirect
)
