# Yapo.cl - Desafio Técnico - Technical Lead

## Tabla de Contenido

- [Requerimientos del taller](https://gitlab.com/assessmentyapocl/ms-backend-api-get-pi/-/blob/develop/doc/Ejercicio_TL_-_Yapo.pdf)
- [Descripción de la solución](#descripción-de-la-solución)
- [Stack Tecnológico](#stack-tecnológico)
- [Consideraciones Funcionales y Técnicas](#consideraciones-funcionales-y-técnicas)
- [Estructura de Directorios](#estructura-de-directorios)
- [Estandarización de Logs](#estandarización-de-logs)
- [Implementación del CI/CD](#implementación-del-cicd)
- [Diagrama de Deploy Lógico de la Solución](#diagrama-de-deploy-lógico-de-la-solución)
- [Parámetros de configuración (CI/CD Variables)](#parámetros-de-configuración-cicd-variables)
- [Instalación de la API en Ambiente Local Dockerizado](#instalación-de-la-api-en-ambiente-local-dockerizado)
- [Pasos para ejecutar Load Test & Performance con jMetter](#pasos-para-ejecutar-load-test-performance-con-jmetter)
- [Seguridad a la API (Preguntas - Item 3 del enunciado)](#seguridad-a-la-api-preguntas-item-3-del-enunciado)
- [Escenario de alto uso de la API (Preguntas - Item 7 del enunciado)](#escenario-de-alto-uso-de-la-api-preguntas-item-7-del-enunciado)
- [Diseño de componentes (Ejercicio 2 del enunciado)](#diseño-de-componentes-ejercicio-2-del-enunciado)
- [Licencia](#licencia)
- [Autor](#autor)

## Descripción de la solución

El test hace referencia a la construcción de una API, que dado un determinado número (enviado como parámetro) servirá de rango para el calculo de un número pseudo-aleatorio que llamaremos X, se debe calcular la cantidad de X decimales para el valor de PI. Para resolver el mismo, se realizó el calculo de decimales de PI basado en el algoritmo de las [formulas de Machin](https://es.wikipedia.org/wiki/F%C3%B3rmulas_de_Machin) ya que es, uno de los algoritmos mas utilizados para calcular con eficiencia la precisión de estos valores decimales. 

Para resolver el problema se implemento el siguiente método:

- GetValuePI : Esta API emplea el método http GET para devolver los N decimales del valor PI. Los valores que devolverá dicha API son una notación de JavaScript Object Notation. (JSON):

    - param  : Le devuelve al usuario el número ingresado.
    - random : El número aleatorio entre 2 y el número ingresado por el usuario y que determinará la cantidad de decimales que devolverá para PI.
    - PiCalc : Este valor hace referencia al valor de PI calculado.

## Stack Tecnológico

- La implementación del desarrollo se llevo a cabo con el lenguaje de programación [Go](https://golang.org/), en su versión 1.16.4 y con la implementación de módulos para gestión de paquetes.
- Para el proyecto se importo una librería que sirve como enrutador y dispatcher llamado [MUX](https://github.com/gorilla/mux)  y que forma parte de la suite de [Gorilla Web Toolkit](https://www.gorillatoolkit.org/)
- Se importó la librería [godotenv](https://github.com/joho/godotenv) para el manejo de variables de entornos. Sea desde un fichero .env o desde valores definidos en un pipeline.
- Se importa librería [logrus](https://github.com/sirupsen/logrus) que es un logger estructurado para Golang, compatible totalmente con la librería nativa logger.
- Se aplica el uso de Kubernetes (GKE) en un proyecto privado de GCP donde se implemento el despliegue en un clúster configurado para efecto de esta prueba. Si se requiere solicitar acceso favor hacerlo a través de un correo electrónico, ello con la finalidad de dar acceso con perfil viewer al mismo.
- Se creo un script (Concurrente) haciendo uso de [Apache jMetter](https://jmeter.apache.org/), con la finalidad de verificar el performance y Load Testing de la Api Restfull.

## Consideraciones Funcionales y Técnicas
* Se entrega [Collection Postman](https://gitlab.com/assessmentyapocl/ms-backend-api-get-pi/-/blob/develop/postman/Assessment_-_TL_-_Yapo_-_postman_collection.json) como cliente Rest para realizar pruebas tanto en ambiente de desarrollo (Local) como ambiente entregado (GKE).
* La implementación para despliegue es a través del uso del CI/CD de Gitlab, poniendo en funcionamiento el uso de makefile.
* No se contemplo uso ningún Framework de Go.
* Se implemento el estrategia de branching [GitFlow](https://www.atlassian.com/es/git/tutorials/comparing-workflows/gitflow-workflow). Por ende encontrará solo dos branch por la actividad desarrollada, el uso de 'develop' como rama de desarrollo y 'master' como rama productiva.
* Se implemento el uso de logs estandarizados en la salida (stdout) de la API.

## Estructura de Directorios
Los directorios del desarrollo de la librería se organizan de la siguiente manera:

**GKE       :** Directorio creado y necesario que contiene los archivos de deployment, configmap y autoscaling del aplicativo.

**api       :** Directorio creado y necesario donde se desarrollo la lógica de negocio de la API desarrollada.

**doc       :** Directorio con la documentación, diseños e imágenes del aplicativo. Si bien no es correcto subir ficheros binarios, es necesario para complementar la documentación del proyecto entregado.

**model     :** Directorio que gestiona las estructuras de las abstracciones a implementar. 

**postman   :** Directorio donde se almacena la colección postman (Cliente Rest) para poder interactuar con las APIs.

**public    :** Directorio de funciones anexas necesarias para el desarrollo del taller.

**jMetter   :** Directorio con script de trabajo del test realizado en jMetter. Así como un informe comprimido de una prueba realizada con un request de 650 Hilos, que en un periodo de 10 segundos realizan peticiones a la API desarrollada con una repetición de 5 veces.

## Estandarización de Logs

Con la finalidad de llevar a la practica logs estandarizados no se empleo la salida estándar de errores de Go. Esto trae como beneficio que se notifica todos los eventos y/o acciones que van ocurriendo en la ejecución del aplicativo y que posteriormente pueden facilitar la ingesta, procesamiento y generación de valor de la información contenida en los logs gracias al uso de herramientas de big data.

El formato de log para la actividad es: 

![](doc/imagenes/img_log.png)

## Implementación del CI/CD

Con la finalidad de llevar a cabo el CI/CD haciendo uso de GitLab para ello. Los script YAML donde se encuentran los job estan desarrollados en [otro proyecto](https://gitlab.com/assessmentyapocl/cicd-template) destinado solo al CI/CD. Los stages implementados son:

**build  :** Script que permite la compilación del proyecto con el propósito de saber si se ha roto o no, el ciclo de compilación del mismo.

**test   :** Script aplicado donde se ejecutaran los test unitarios para aprobar que el cambio realizado no afecto el comportamiento que ha de tener en la API.

**build-docker   :** Script que permite construir el contenedor que permanecerá utilizándose en el despliegue y lo almacenara en el Registry de GCP destinado para este proyecto.

**release   :** Estos scripts (para desarrollo y producción) permiten realizar el 'retagging' de las imagenes subidas y ubicarlas correctamente según el ambiente donde se vaya a deployar. Se sigue haciendo uso del Registry de GCP para esta actividad. 

**deploy   :** El despliegue de software como su nombre indica, permite interactuar y crear los POD correspondientes en el cluster indicado para la actividad. En este Job de igual manera se implementa el HPA (Horizontal Pod Autoscaler).

## Diagrama de Deploy Lógico de la Solución

![](doc/imagenes/img_diagrama_1.png)

![](doc/imagenes/img_diagrama_2.png)

## Parámetros de configuración (CI/CD Variables)

A continuación se explica el uso de cada variable de la configuración:

Variable|Tipo de dato en Go|Observación
---|---|---|
DOCKER_AUTH_CONFIG|String|Configuración de Docker para autenticación|
GCLOUD_CLUSTER|String|Cluster k8s de GCP para el Proyecto|
GCLOUD_PROJECT_ID|String|Nombre del proyecto de GCP|
GCLOUD_ZONE|String|Zone en la cual esta configurada en cluster GCP|
GCP_REGISTRY_CONFIG|String|System Account del Registry de GCP|
K8S_NAMESPACE_DEV |String|Namespace de desarrollo para deployar|
K8S_NAMESPACE_PROD|String|Namespace de producción para deployar|
KUBE_SA_GCP_CONFIG|String|System Account del Kubernetes Admin de GCP|
LANGUAGE|String|Lenguaje de Desarrollo en que esta la implementación|
MAX_RANDOM_PRECISION|String|Valor maximo que puede tomar el parametro random para generar un maximo de decimales para el valor PI|
MODE_DEBUG_DEV|String|Flag que activa/desactiva el Debug del aplicativo en ambiente de desarrollo|
MODE_DEBUG_PROD|String|Flag que activa/desactiva el Debug del aplicativo en ambiente productivo|
PORT_SERVER_DEV|String|Puerto en que esta configurado el server en ambiente de desarrollo|
PORT_SERVER_PROD|String|Puerto en que esta configurado el server en ambiente de desarrollo|
PROJECT_NAME|String|Nombre de los workloads en GCP|

## Instalación de la API en Ambiente Local Dockerizado

La explicación de como instalar el aplicativo en un ambiente dockerizado parte por iniciar en un equipo (linux, Windows, Mac), el mismo debe tener los siguientes aplicativos instalados previamente:

- Docker.
- git.

Una vez ingrese deberá seguir los siguientes pasos al pie de la letra para que funcione:

1. Abrir una terminal.
2. Crear el directorio donde dejara el código fuente del proyecto:
```bash
mkdir -p ~/microservicios
cd ~/microservicios
```
3. Clonar el repositorio del proyecto.
```bash
git clone https://gitlab.com/assessmentyapocl/ms-backend-api-get-pi.git
```
4. Ingresar credenciales de acceso al gitlab con su respectivo user y password.
5. Moverse al directorio recien descargado con el comando 'cd'.
```bash
cd ms-backend-api-get-pi/
```
6. Generar la Imagen del servidor de golang.
```bash
docker image build -t svrgolang:1.0.0 . --no-cache=true
```
7. Una vez termine de instalar el paso a paso del Dockerfile con el comando anterior, validar que efectivamente se creo la imagen de forma correcta, con el comando:
```bash
docker image ls
```
8. Ejecutar un contenedor con la imagen recién creada:
```bash
docker run -d -p 3001:3001 --name svrgolang-yapo -d svrgolang:1.0.0
```
9. Verificar que el contenedor efectivamente esta trabajando con el comando:
```bash
docker container ls
```
10. Si desea activar y/o ver los logs del microservicio ingrese el siguiente comando: 
```bash
vim cfg/config.go
```
11. Generar la Imagen del servidor go.
```bash
docker build -t svr_golang:0.0.1 --target svrgo .
```
12. Se crea el Contenedor de go y automáticamente levanta el aplicativo mostrando los logs del aplicativo.
```bash
docker logs svrgolang-yapo
docker logs --follow svrgolang-yapo # -- Si desea hacer un monitoreo continuo
```
13. Puede ingresar a cualquier navegador sabiendo la ip y el puerto (debería ser por default el 3001) e ingresar la siguiente URL: 
```bash
http://192.168.108.209:3001/api/v1/getPI/100
```
```diff
- Importante : 
    - El valor '100' es el parámetro que usted puede ir modificando.
    - El equipo que hace de docker host, debe tener los firewall sobre el puerto 3001 habilitado o hacer la 
    redirección correspondiente al ejecutar el paso 8.
```

## Pasos para ejecutar Load Test & Performance con jMetter
1. Descargar y Ejecutar [Apache jMetter](https://jmeter.apache.org/download_jmeter.cgi).
2. Abrir una terminal.
3. Crear el directorio donde dejara el código fuente del proyecto:
```bash
mkdir -p ~/microservicios
cd ~/microservicios
```
4. Clonar el repositorio del proyecto.
```bash
git clone https://gitlab.com/assessmentyapocl/ms-backend-api-get-pi.git
```
5. Ingresar credenciales de acceso al gitlab con su respectivo user y password.
6. Moverse al directorio donde esta el fichero jMetter, con el comando 'cd':
```bash
cd ms-backend-api-get-pi/jmetter/
```
7. En la aplicación jMetter abrir y buscar el proyecto jmetter como se observa en la siguiente imagen:
![](doc/imagenes/img_abrir_jmetter.png)
```diff
- Importante : 
    - En proyecto jMetter lo conseguirá en la misma ruta indicada en el item 6.
```
8. Una vez abierto el proyecto jmetter, debe indicar los parámetros necesarios para ejecutar la prueba.
    - Número de Hilos : Indica la cantidad de Hilos que concurrentemente harán los request. (Por default esta cargado el valor 650)
    - Periodo de Subida : Indica el tiempo en segundos que concurrentemente deben enviarse los "Numeros de Hilos". (Por default esta en 2)
    - Contador del Bucle : Cantidad de veces a repetir el ciclo del Parámetro "Numero de Hilo". (Por default esta en 10)
Ver la siguiente imagen:
![](doc/imagenes/configura-jmetter.png)
```diff
- Importante : 
    - Si desea modificar los parámetros del request HTTP que se llama: "request-ms-backend-api-get-pi". 
    Considerar:
    - El parámetro que se envía es, siempre aleatorio según la configuración que tiene el jMetter.
    - El Número enviado debe ser entre 1 y 350000 puesto que los tiempos de respuesta al exigir tal precisión son excesivamente lentos y por ende se configuro en el CONFIGMAP ese valor si se desea un valor diferente se debe cambiar en el CI/CD y redeployar el microservicio.
```
9. Si desea ejecutar la prueba solo debe dar Play a la misma, y el resultado final lo puede ver en los informes de resultados, como se aprecia en la siguiente imagen:
![](doc/imagenes/result_jmetter.png)

## Seguridad a la API (Preguntas - Item 3 del enunciado)

1. ¿Qué componentes usarías para securitizar tu API?

Es destacable que exponer una API sin si quiera un método de autenticación es, un peligro ya que es vulnerable para ataques. Es por ello que:

- Se hace necesario implementar un API Gateway con la finalidad de aprovechar plugins necesarios para agregar un poco de seguridad al desarrollo. Pudiendo separar la Autenticación de la Autorización.
- Se debe establecer un método de autenticación (Sea al menos una apiKey o un Json Web Token -JWT- o oauth2 - teniendo claro que oauth2 no es un método de autenticación-) que permita autenticar/autorizar a los consumidores de la API.
- En los plugins que se implementarían por ejemplo: acl (Estableciendo y filtrando quienes serian mis consumidores), request-transformer, response-transformer, rate-limiting (para poder indicar un máximo de HTTP Request que deberia aceptar la API por segundo)

2. ¿Como asegurarías tu API desde el ciclo de vida de desarrollo?

- Definiendo en la fase diseño/planificación el método de autenticación a utilizar y saber que desarrollos puede conllevar según el método de autenticación a implementar.
- Si la data a enviar/recibir es extremadamente sensible emplearía una librería de criptografía con secretos. (Aquí viene un análisis de data en transito y/o persistida)
- Contar e involucrar un recurso de DevSecops desde el inicio del proyecto con la finalidad de mejorar e implementar stages de seguridad al Pipeline.
- Planificaría desde un principio un Ethical Hacking (involucrando a un equipo de seguridad): Inyección SQL, Cross-site scripting, Análisis de trafico, Protección en la capa de transporte, configuraciones, entre otras. Pudiendo detectar las vulnerabilidades encontradas y que sean mitigadas antes de la liberación de un release.
- Aplicaría de igual manera el hardening del contenedor empleado para deployar la solución.
- Como buena práctica aplicaría la API Health con la finalidad de conocer el estado operativo del microservicio desarrollado.

## Escenario de alto uso de la API (Preguntas - Item 7 del enunciado)

Para realizar un escalamiento horizontal de PODs (HPA) implementaría un script de autoscaling al pipeline para que en el stage de deploy permita el crecimiento o despliegue de mas pods según parámetros establecidos. Por ejemplo un uso excesivo de procesamiento, memoria, etc...

El script necesario lo conseguirá en el script de [autoscaling.yml](https://gitlab.com/assessmentyapocl/ms-backend-api-get-pi/-/blob/develop/GKE/autoscaling.yml) y que esta en el CI/CD del proyecto. 

## Diseño de componentes (Ejercicio 2 del enunciado)
1. Imagínate que quieres exponer la API que desarrollaste en el ejercicio anterior. ¿Cómo sería ese diagrama de componentes de alto nivel (imagina que esa API la publicaras en un servicio de cloud computing)?.
![](doc/imagenes/img_diagrama_3.png)

2. Imagínate que quieres exponer la API que desarrollaste en el ejercicio anterior. ¿Cómo sería ese diagrama de componentes de alto nivel (imagina que esa API la publicaras en un servicio de cloud computing)?.
![](doc/imagenes/img_diagrama_4.png)

3. Supongamos que necesitamos instalar una aplicación web legado en una VM. ¿Puedes dibujar un diagrama de componentes para mostrar cómo expondrías el servicio web hacia el exterior?
![](doc/imagenes/img_diagrama_5.png)

## Licencia
[GPLv3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Copyleft.svg/220px-Copyleft.svg.png" width="25px" height="25px" style="display:inline;margin:0"> Copyleft | Yapo - 2021

## Autor

Rodrigo G. Higuera M. <<rodrigoghm@gmail.com>>
